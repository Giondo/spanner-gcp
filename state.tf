terraform {
  backend "gcs" {
    bucket  = "equifax-demo-tfstate"
    prefix  = "terraform/state/spanner-gcp"
  }
}
